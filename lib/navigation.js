
var GraphiteUtil = GraphiteUtil || {};

GraphiteUtil.navigation = {
    launchGraphiteWithScreen: function (xmlString, graphiteSettings) {
        return this.setScreen(xmlString, graphiteSettings)
            .then(function (response) {
                // Simple check to ensure server has provided a valid response
                return response.json();
            })
            .then(function () {
                return this.navigateToGraphite(graphiteSettings);
            }.bind(this));
    },

    setScreen: function (xmlString, graphiteSettings) {
        var setFileSourceURL = graphiteSettings.baseURL + 'setFileSource';

        var formData = new FormData();
        formData.append('application', graphiteSettings.application);
        formData.append('dir', 'screens');
        formData.append('file', graphiteSettings.screen.split('/').pop());
        formData.append('src', xmlString);

        var fetchCfg = {
            method: 'POST',
            body: formData
        };

        return fetch(setFileSourceURL, fetchCfg);
    },

    navigateToGraphite: function (graphiteSettings) {
        var graphiteURL = graphiteSettings.baseURL + 'graphite?application=' + graphiteSettings.application + '#file-path=' + graphiteSettings.screen;

        return this.getTab()
            .then(function (tab) {
                return new Promise (function (resolve) {
                    chrome.tabs.update(tab.id, {active: true, url: graphiteURL}, resolve);
                });
            });
    },

    getTab: function () {
        return GraphiteUtil.settings.getTabID()
            .then(function (tabID) {
                // Create tab if one was never stored
                if (tabID == null) {
                    return this.makeTab();
                }

                // Otherwise find the stored tab
                return new Promise(function (resolve) {
                    chrome.tabs.get(tabID, resolve);
                });
            }.bind(this))
            .then(function (tab) {
                // The stored tab may have been closed. If do, make a new one
                if (tab == null) {
                    return this.makeTab();
                }

                return tab;
            }.bind(this));
    },

    makeTab: function () {
        return new Promise(function (resolve) {
            chrome.tabs.create({}, function (tab) {
                GraphiteUtil.settings.setTabID(tab.id);
                resolve(tab);
            });
        });
    }
};
