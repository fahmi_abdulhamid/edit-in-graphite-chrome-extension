
var GraphiteUtil = GraphiteUtil || {};

GraphiteUtil.settings = {
    getGraphiteSettings: function () {
        const storageArea = this.getStorageArea();
        return new Promise (function (resolve) {
            const keys = ['baseURL', 'application', 'screen'];
            return storageArea.get(keys, function (items) {
                resolve (items);
            });
        });
    },

    setGraphiteSettings: function (urlString) {
        var url = new URL(urlString);

        var screen = 'screens/main.xml';
        if (url.hash.includes('file-path')) {
            screen = decodeURIComponent(url.hash.replace('#file-path=', ''));
        }

        var baseURL = (url.origin + url.pathname).replace(/\w+$/, '');
        var application = url.searchParams.get('application');
        var settings = {
            baseURL: baseURL,
            application: application,
            screen: screen
        };

        alert(JSON.stringify(settings, null, 4));

        this.getStorageArea().set(settings);
    },

    setLastXML: function (xmlString) {
        this.getStorageArea().set({lastXML: xmlString});
    },

    getLastXML: function () {
        const storageArea = this.getStorageArea();

        return new Promise(function (resolve) {
            storageArea.get('lastXML', function (items) {
                resolve(items.lastXML);
            });
        });
    },

    hasLastXML: function () {
        return this.getLastXML()
            .then(function (lastXML) {
                return !!lastXML;
            });
    },

    hasSettings: function (graphiteSettings) {
        return !!graphiteSettings.baseURL;
    },

    getTabID: function () {
        const storageArea = this.getStorageArea();

        return new Promise(function (resolve) {
            storageArea.get('tabID', function (items) {
                resolve(items.tabID);
            });
        });
    },

    setTabID: function (tabID) {
        const storageArea = this.getStorageArea();

        return new Promise(function (resolve) {
            storageArea.set({tabID: tabID}, resolve);
        });
    },

    getStorageArea: function () {
        return chrome.storage.sync;
    },
};
