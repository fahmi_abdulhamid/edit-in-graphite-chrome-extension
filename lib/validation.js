
var GraphiteUtil = GraphiteUtil || {};

GraphiteUtil.validation ={
    isGraphiteURL: function (urlString) {
        var url = new URL(urlString);
        return url.pathname.includes('/graphite');
    },

    isValidScreenXML: function (xmlString) {
        var domParser = new DOMParser();
        var xml = domParser.parseFromString(xmlString, 'application/xml');
        return xml.documentElement.nodeName === 'screen';
    }
};
