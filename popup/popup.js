function repeat() {
    Promise.all([GraphiteUtil.settings.getGraphiteSettings(), GraphiteUtil.settings.getLastXML()])
        .then(function (values) {
            var graphiteSettings = values[0];
            var lastXML = values[1];

            return GraphiteUtil.navigation.launchGraphiteWithScreen(lastXML, graphiteSettings);
        })
        .then(closePopup)
        .catch(function (e) {
            alert(e.message);
        });
}

function setURL() {
    chrome.tabs.getSelected(function (tab) {
        GraphiteUtil.settings.setGraphiteSettings(tab.url);
        GraphiteUtil.settings.setTabID(tab.id);

        closePopup();
    });
}

function closePopup() {
    window.close();
}

function bind() {
    var repeatButton = document.getElementById('repeat-button');
    repeatButton.addEventListener('click', repeat);

    var setButton = document.getElementById('set-button');
    setButton.addEventListener('click', setURL);

    chrome.tabs.getSelected(function (tab) {
        var isGraphiteURL = GraphiteUtil.validation.isGraphiteURL(tab.url);
        setButton.disabled = !isGraphiteURL;
        repeatButton.disabled = !isGraphiteURL;

        if (isGraphiteURL) {
            GraphiteUtil.settings.hasLastXML()
                .then(function (hasLastXML) {
                    repeatButton.disabled = !hasLastXML;
                });
        }
    });
}

// Start
window.onload = bind;

