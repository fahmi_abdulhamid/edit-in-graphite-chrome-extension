
function getSelectedText() {
    // Retrieve text from window.getSelection() to include line breaks
    // See: https://bugs.chromium.org/p/chromium/issues/detail?id=116429
    return new Promise(function (resolve) {
        chrome.tabs.executeScript( {
            code: "window.getSelection().toString();"
        }, function(selection) {
            var text = selection[0];
            text = text.replace(/\u00a0/g, ' '); // Convert "&nbsp;" to " "
            resolve(text);
        });
    });
}

function editInGraphite(info) {
    getSelectedText()
        .then(function (text) {
            if (text != null && GraphiteUtil.validation.isValidScreenXML(text)) {
                // Remember XML for "repeat" button in popup
                GraphiteUtil.settings.setLastXML(text);

                return GraphiteUtil.settings.getGraphiteSettings()
                    .then(function (graphiteSettings) {
                        return GraphiteUtil.navigation.launchGraphiteWithScreen(text, graphiteSettings);
                    });
            }
        })
        .catch(function (e) {
            alert(e.message);
        });
}

function makeContextMenu(enabled) {
    chrome.contextMenus.create({
        title: 'Edit in Graphite',
        contexts:  ['selection'],
        onclick: editInGraphite,
        enabled: enabled
    });
}

// Start
GraphiteUtil.settings.getGraphiteSettings()
    .then(GraphiteUtil.settings.hasSettings)
    .then(makeContextMenu);
